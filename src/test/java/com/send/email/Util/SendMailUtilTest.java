package com.send.email.Util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.*;

/**
 * @author Csea
 * @title
 * @date 2019/9/13 12:07
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SendMailUtilTest {
    @Resource
    SendMailUtil sendMailUtil;

    @Test
    public void sendSimpleEmail() {
        sendMailUtil.sendSimpleEmail("2420221622@qq.com", "springboot邮件主题", "Springboot邮件内容");
    }

    @Test
    public void sendHtmlMail() throws MessagingException {
        sendMailUtil.sendHtmlMail("2420221622@qq.com", "springboot-HTML邮件主题", "<h1>内容：第一封html邮件</h1>");
    }

    @Test
    public void sendAttachmentsMail() throws MessagingException {
        String filePath = "C:/Users/CSea/Desktop/新建文本文档.zip";
        sendMailUtil.sendAttachmentsMail("2420221622@qq.com", "springboot-附件邮件", "这是带附件的邮件", filePath);
    }

    @Test
    public void sendInlineResourceMail() throws MessagingException {
        String srcPath = "C:/Users/CSea/Desktop/icon.jpg";
        String srcId = "icon";
        String content = "<h1 style='color:red'>helloWorld</h1><img src='cid:" + srcId + "'/><img src='cid:" + srcId + "'/>";
        sendMailUtil.sendInlineResourceMail("2420221622@qq.com", "springboot-图片邮件", content, srcPath, srcId);
    }

    @Test
    public void testTemplate() throws IOException, TemplateException, MessagingException {
        sendMailUtil.sendHtmlMail("2420221622@qq.com", "springboot-图片邮件", sendMailUtil.testTemplate("张三"));
    }
}