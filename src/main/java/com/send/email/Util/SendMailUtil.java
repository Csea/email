package com.send.email.Util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Csea
 * @title
 * @date 2019/9/12 16:55
 */
@Service
public class SendMailUtil {
    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private Configuration configuration;

    //文本邮件
    public void sendSimpleEmail(String to, String subject, String content) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(to);
        mailMessage.setSubject(subject);
        mailMessage.setText(content);
        mailMessage.setFrom(from);
        mailSender.send(mailMessage);
    }

    //HTML邮件
    public void sendHtmlMail(String to, String subject, String content) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(content, true);
        mailSender.send(mimeMessage);
    }

    //附件邮件
    public void sendAttachmentsMail(String to, String subject, String content, String filePath) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setTo(to);
        helper.setFrom(from);
        helper.setSubject(subject);
        helper.setText(content, true);

        FileSystemResource file = new FileSystemResource(new File(filePath));
        String fileName = file.getFilename();
        helper.addAttachment(fileName, file);
        helper.addAttachment(fileName + "log", file);
        mailSender.send(mimeMessage);
    }

    //图片邮件
    public void sendInlineResourceMail(String to, String subject, String content, String srcPath, String srcId) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom(from);
        helper.setText(content, true);
        helper.setSubject(subject);
        helper.setTo(to);

        FileSystemResource res = new FileSystemResource(new File(srcPath));
        helper.addInline(srcId, res);
        helper.addInline(srcId, res);
        mailSender.send(mimeMessage);
    }

    //模板邮件(文字)
    public String testTemplate(String name) throws IOException, TemplateException, MessagingException {
        String htmlText = "";
        Template template = configuration.getTemplate("email.ftl");
        Map map = new HashMap();
        map.put("user", name);
        htmlText = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
        return htmlText;
    }
}
